import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import session from 'express-session';
import {Queue} from '@src/services/queue';
import {converter} from './services/converter';
import router from '@src/routes';

const app = express();
const queue = new Queue(converter);
app.use(cors());
app.use(
  session({
    secret: 'foieferfjpk43-0j3xj39i-0dk30kd-3jg295gj-jf0ji2jeje',
    resave: true,
    saveUninitialized: true,
  })
);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(queue.use());
app.use(router);
app.use(express.static('uploads'));
app.use(morgan('dev'));

export default app;
export {app, queue};
