'use strict';

/**
 * Module dependencies.
 */

import app, {queue} from '@src/bootstrap';

app.listen(3000, () => {
  queue.run();
  console.log('🚀 Express started on port 3000');
});
