import multer from 'multer'
import express, {Request, Response} from 'express';
import { Queue } from '@src/services/queue';
import { UPLOAD_DIR } from '@src/settings';

const router = express.Router();
const upload = multer({ dest: UPLOAD_DIR })

router.post('/files/upload', upload.single('file'), Queue.useQueueFile(), async (req: Request, res: Response) => {
  req.token ? res.json({token: req.token}) : res.json({error: 'Something went wrong.'});
});

router.get('/files/:token', async (req: Request, res: Response) => {
  const result = await req.queue?.getTokenResult(req.params.token);
  res.json(result ? result : {status: 'NOTFOUND'});
});

export default router;
