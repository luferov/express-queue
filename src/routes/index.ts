import express from 'express';
import filesRoutes from './files'

const router = express.Router();

router.get('/', (req, res) => {
  res.send('Hello, world!');
});

router.get('/healthcheck', (req, res) => {
  res.send('OK');
});

router.use(filesRoutes);

export default router;
