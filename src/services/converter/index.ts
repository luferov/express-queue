import path from 'path';
import fs from 'fs/promises'
import sharp, {AvailableFormatInfo, FormatEnum} from 'sharp';
import {getAbsolutePath} from '@src/services/queue/utils';

export async function converter(
  filepath: string,
  formatFrom: string,
  formatTo: keyof FormatEnum | AvailableFormatInfo,
  file: string
): Promise<string> {
  const filePathAbsolute = await getAbsolutePath(filepath);
  await sharp(filePathAbsolute).toFormat(formatTo).toFile(file);
  await fs.unlink(filePathAbsolute);
  return file;
}
