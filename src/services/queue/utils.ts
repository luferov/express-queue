import path from 'node:path';
import * as crypto from 'crypto';
import {ParsedPathType} from './types';

/**
 * Make absolute path
 * @param filePath file or folder path
 * @returns absolute path
 */
export async function makeAbsolutePath(
  filePath: string
): Promise<ParsedPathType> {
  const parsedPath = path.parse(filePath);
  const root = parsedPath.root || process.cwd() + '/';
  return {
    path: path.format({
      ...parsedPath,
      root,
    }),
    name: parsedPath.name,
    isFile: parsedPath.ext !== '',
    ext: parsedPath.ext,
    root,
  };
}

export async function getAbsolutePath(
  filePath: string
): Promise<string> {
  const parsedPath = path.parse(filePath);
  const root = parsedPath.root || process.cwd() + '/';
  return path.format({
    ...parsedPath,
    root,
  })
}

export async function buildFilename(
  root: string,
  name: string,
  ext: string
): Promise<string> {
  return path.format({
    root,
    name,
    ext,
  });
}
/**
 * Make unique char sequence
 * @returns Unique char sequence
 */
export async function hashedName(): Promise<string> {
  return crypto.createHash('md5').update(Date.now().toString()).digest('hex');
}
