import {AvailableFormatInfo, FormatEnum} from 'sharp';

/**
 * Element state in queue.
 */
export type TransformItem = {
  token: string;
  filepath: string;

  formatFrom: string;
  formatTo: keyof FormatEnum | AvailableFormatInfo,
  file: string;
  quality: number;

  [k: string]: unknown;
};

/**
 * Collection of elements with user session.
 */
export type TState = {
  [k: string]: TransformItem[];
};
/**
 * Result item converting
 */
export type TResultItem = {
  file: string;
  status: 'WAIT' | 'PENDING' | 'DONE' | 'ERROR';
};
/**
 * Collection of results of queue
 */
export type TResult = {
  [k: string]: TResultItem;
};
/**
 * Executor function format.
 */
export type TExecutor = (
  filepath: string,
  formatFrom: string,
  formatTo: keyof FormatEnum | AvailableFormatInfo,
  file: string,
  quality?: number
) => Promise<string>;

/**
 * Path parsed file type.
 */
export type ParsedPathType = {
  root: string;
  path: string;
  name: string;
  isFile: boolean;
  ext: string;
};
