import path from 'path';
import {Request, Response, NextFunction} from 'express';
import {AvailableFormatInfo, FormatEnum} from 'sharp';
import {buildFilename, hashedName, makeAbsolutePath} from './utils';
import {TState, TResult, TResultItem, TExecutor, TransformItem} from './types';
import { UPLOAD_DIR } from '@src/settings';

/**
 * Queue
 * Need use two states.
 *   - state - for queue: 3 users 1 files in parallels.
 *   - results - flat results of queue operations.
 */
export class Queue {
  private executor: TExecutor;
  private state: TState = {};
  private results: TResult = {};
  private size: number = 0;
  private maxSize: number = 100;

  constructor(executor: TExecutor, maxSize: number = 100) {
    this.executor = executor;
    this.maxSize = maxSize;
  }
  /**
   * Running queue
   */
  async run() {
    while (true) {
      const tasks: TransformItem[] = [];
      for (const userId of Object.keys(this.state).slice(0, 3)) {
        const taskItem = this.state[userId].splice(0, 1)[0];
        tasks.push(taskItem);
        this.results[taskItem.token].status = 'PENDING';
      }

      const resultOfTasks = await Promise.all(
        tasks.map(task =>
          this.executor(
            task.filepath,
            task.formatFrom,
            task.formatTo,
            task.file,
            task.quality
          )
        )
      );

      for (const i in tasks) {
        this.results[tasks[i].token] = {file: resultOfTasks[i], status: 'DONE'};
      }
      this.size -= resultOfTasks.length;
      this.state = Object.keys(this.state).reduce((a, userId) => {
        if (this.state[userId].length > 0) {
          return {...a, [userId]: this.state[userId]};
        }
        return a;
      }, {});
      await new Promise(r => setTimeout(r, 100));
    }
  }
  /**
   * Middleware to inject queue in req.
   * @param req Request
   * @param res Response
   */
  use() {
    return (req: Request, res: Response, next: NextFunction) => {
      req.queue = this;
      next();
    };
  }
  /**
   * Middleware to add req.file to queue. Need use after multer and queue.use().
   * @param req Request
   * @param res Response
   */
  static useQueueFile() {
    return async (req: Request, res: Response, next: NextFunction) => {
      if (req.queue && req.file) {
        const formatFrom = path.extname(req.file.originalname).slice(1);
        req.token = await req.queue.add(
          req.sessionID,
          req.file.path,
          formatFrom,
          req.body.format
        );
      }
      next();
    };
  }
  /**
   * Add task to queue
   * @param userId - session user id
   * @param filepath
   * @param formatFrom
   * @param formatTo
   * @returns token
   */
  async add(
    userId: string,
    filepath: string,
    formatFrom: string,
    formatTo: keyof FormatEnum | AvailableFormatInfo = 'webp',
    quality: number = 80
  ): Promise<string | null> {
    if (this.size > this.maxSize) {
      return null;
    }
    if (!(userId in this.state)) {
      this.state[userId] = [];
    }
    const {root, name, ...propsPath} = await makeAbsolutePath(filepath);
    const token = await hashedName();
    const file = await buildFilename(root + UPLOAD_DIR, name, `${formatTo}`);
    this.state[userId].push({
      token,
      filepath,
      formatTo,
      formatFrom,
      file,
      quality,
    });
    this.size += 1;
    this.results[token] = {file, status: 'WAIT'};
    return token;
  }
  /**
   * Get result of token.
   * @param token
   * @returns
   */
  async getTokenResult(token: string): Promise<TResultItem | undefined> {
    return this.results[token];
  }
}
