import {Queue} from './services/queue';

declare global {
  namespace Express {
    interface Request {
      queue?: Queue
      token?: string | string[] | null
    }
  }
}